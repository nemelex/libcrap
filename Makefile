check_all:
	python3 -m pytest

upload:
	python3 setup.py sdist && \
	  python3 setup.py bdist_wheel && \
	  twine upload dist/* && \
	  rm -rf dist/*

.PHONY: run_tests check_all upload
