from libcrap.torch.training import get_model_name

import torch.nn as nn

def test_get_model_name():
    assert get_model_name(nn.Linear(11, 22)) == "linear"

    class FooBarBuzz(nn.ReLU):
        pass

    assert get_model_name(FooBarBuzz()) == "foobarbuzz"
