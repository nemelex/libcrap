from libcrap.core import *

import os
import pickle
from itertools import chain
import re


def test_map_thread_parallel():
    for use_progress_bar in [True, False]:
        assert [chr(i) for i in range(50)] == \
            map_thread_parallel(chr, range(50), use_progress_bar)


def test_save_and_load_json(tmpdir):
    filename = os.path.join(tmpdir.strpath, "sample.json")
    json_struct = {
        'Hello world': {
            'a': [1, 2, 3, "ohoh"]
        },
        'aaaa': 'bbb'
    }
    save_json(json_struct, filename)
    loaded = load_json(filename)
    assert json_struct == loaded


def test_shuffled():
    numbers = list(range(100))
    shuffled_numbers = shuffled(numbers)
    assert frozenset(shuffled_numbers) == frozenset(numbers)

    # this might happen with extremely low probability
    assert shuffled_numbers != numbers


def test_traverse_files(tmpdir):
    files = [
        tmpdir.join("somefile.txt"),
        tmpdir.mkdir("subdir1").join("asdasddsa"),
        tmpdir.join("anotherfileo"),
        tmpdir.mkdir("subdir2").join("asdasd.o")]

    for file_ in files:
        file_.write("asdasd")

    filenames = frozenset(traverse_files(tmpdir.strpath))
    real_filenames = frozenset(file_.strpath for file_ in files)
    assert filenames == real_filenames

def test_traverse_files_no_recursion(tmpdir):
    included_files = (
        tmpdir.join("file.mp4"),
        tmpdir.join("anotherfile.sssss")
    )
    excluded_files = (
        tmpdir.join("abc"),
        tmpdir.mkdir("subdir1").join("file.mp4"),
        tmpdir.mkdir("subdir2").join(".ggg"),
        tmpdir.join("asd.txt")
    )
    for file_ in chain(included_files, excluded_files):
        file_.write("asdasd")
    result = tuple(traverse_files_no_recursion(tmpdir, ("mp4", "sssss")))
    for file_ in included_files:
        assert file_ in result
    for file_ in excluded_files:
        assert file_ not in result


def test_calcsave_or_load(tmpdir):
    filename = tmpdir.join("test_calcsave_or_load.pkl").strpath

    def load(filename):
        with open(filename, 'rb') as file:
            return pickle.load(file)

    def save(obj, filename):
        with open(filename, 'wb') as file:
            pickle.dump(obj, file)

    counter = 0

    @calcsave_or_load(filename, load, save)
    def build_obj():
        nonlocal counter
        counter += 1
        return {"kappa": "keepo", 1: [3, 15, 16]}

    # build the object and save it
    obj1 = build_obj()
    assert counter == 1
    assert obj1["kappa"] == "keepo"

    # load object
    obj2 = build_obj()
    assert counter == 1
    assert obj1 == obj2


def test_json_calcsave_or_load(tmpdir):
    filename = tmpdir.join("test_json_calcsave_or_load.json").strpath

    counter = 0

    @json_calcsave_or_load(filename)
    def build_obj():
        nonlocal counter
        counter += 1
        return {"kappa": "keepo", '11': [3, 15, 16]}

    # build the object and save it
    obj1 = build_obj()
    assert counter == 1
    assert obj1["kappa"] == "keepo"

    # load object
    obj2 = build_obj()
    assert counter == 1
    assert obj1 == obj2


def test_get_now_as_str():
    simplest = get_now_as_str(utc=False)
    assert re.match("^\d\d-\d\dT\d\d:\d\d$", simplest) is not None

    utc_year_seconds = get_now_as_str(year=True, seconds=True)
    assert re.match("^UTC\d\d\d\d-\d\d-\d\dT\d\d:\d\d:\d\d$", utc_year_seconds) is not None


def test_FunctionWithEvents():
    def make_callable_and_checker():
        received_args = None
        received_kwargs = None
        def func(*args, **kwargs):
            nonlocal received_args, received_kwargs
            received_args = args
            received_kwargs = kwargs
        def check(expected_args, expected_kwargs):
            assert expected_args == received_args
            assert expected_kwargs == received_kwargs
        return func, check

    def function_to_wrap(a, b, **kwargs):
        assert a in ("a", "A")
        assert b in ("b", "B")
        assert tuple(kwargs.items()) in ((("c", "c"), ), (("c", "C"), ))
        return "d"

    before1, check_before1 = make_callable_and_checker()
    after1, check_after1 = make_callable_and_checker()
    function_with_events = FunctionWithEvents(
        function_to_wrap, before_call=[before1], after_call=[after1]
    )
    assert function_with_events("a", b="b", c="c") == "d"
    check_before1(("a", ), {"b": "b", "c": "c"})
    check_after1(("d", "a"), {"b": "b", "c": "c"})

    before2, check_before2 = make_callable_and_checker()
    after2, check_after2 = make_callable_and_checker()
    function_with_events.before_call.append(before2)
    function_with_events.after_call.append(after2)

    assert function_with_events("A", b="B", c="C") == "d"
    check_before1(("A", ), {"b": "B", "c": "C"})
    check_before2(("A", ), {"b": "B", "c": "C"})
    check_after1(("d", "A"), {"b": "B", "c": "C"})
    check_after2(("d", "A"), {"b": "B", "c": "C"})
