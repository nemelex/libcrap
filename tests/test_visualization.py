from libcrap.visualization import *


def test_generate_points():
    def sqr(x):
        return x**2

    start = 3.5
    end = 7.0
    num = 6

    results = generate_points(sqr, start, end, num)

    # check that results have correct dimensions
    assert len(results) == 2
    assert all(len(arr) == num for arr in results)

    for (x, y) in zip(results[0], results[1]):
        assert start <= x <= end
        assert y == sqr(x)
