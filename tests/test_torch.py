from libcrap.torch import *

import numpy as np
import random

import torch
import torch.nn as nn
import torch.nn.functional as tnnf

def test_inverse_sigmoid():
    devices_available = [torch.device("cpu")]
    if torch.cuda.is_available():
        devices_available += [torch.device("cuda:0")]
    
    for device in devices_available:
        xs = list(torch.linspace(-5.0, 5.0, steps=33, device=device)) \
            + [torch.randn(1, 2, 3, 4, 5, device=device)]
        for x in xs:
            y = torch.sigmoid(x)
            x_reconstructed = inverse_sigmoid(y)
            assert x_reconstructed.device == device
            assert x_reconstructed.shape == x.shape
            assert torch.all(torch.abs(x_reconstructed - x) <= 1e-2)


def test_set_random_seeds():
    device = torch.device("cuda") if torch.cuda.is_available() else torch.device("cpu")
    set_random_seeds(device)
    x1_random = random.randint(-123456789, 123456789)
    x1_np = np.random.randn(5, 5, 5)
    x1_torch_cpu = torch.rand(10, 3)
    x1_torch_device = torch.randn(10, 3, device=device)
    y1 = torch.rand(10, device=device)
    layer1 = nn.Linear(3, 1).to(device)
    pred1 = layer1(x1_torch_device).reshape(-1)
    loss1 = tnnf.mse_loss(pred1, y1)
    loss1.backward()

    set_random_seeds(device)
    x2_random = random.randint(-123456789, 123456789)
    x2_np = np.random.randn(5, 5, 5)
    x2_torch_cpu = torch.rand(10, 3)
    x2_torch_device = torch.randn(10, 3, device=device)
    y2 = torch.rand(10, device=device)
    layer2 = nn.Linear(3, 1).to(device)
    pred2 = layer2(x2_torch_device).reshape(-1)
    loss2 = tnnf.mse_loss(pred2, y2)
    loss2.backward()

    assert x1_random == x2_random
    assert np.all(x1_np == x2_np)
    for (tensor1, tensor2) in (
        (x1_torch_cpu, x2_torch_cpu),
        (x1_torch_device, x2_torch_device),
        (y1, y2),
        (layer1.weight, layer2.weight),
        (layer1.bias, layer2.bias),
        (pred1, pred2),
        (loss1, loss2),
        (layer1.weight.grad, layer2.weight.grad),
        (layer1.bias.grad, layer2.bias.grad),
    ):
        assert torch.all(tensor1 == tensor2)
